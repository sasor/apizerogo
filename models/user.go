package models

import (
    "errors"
    "github.com/sasor/apizeroingo/utils"
)

type User struct {
    UID       uint   `json:"_id"`
    Nickname  string `json:"nickname"`
    Email     string `json:"email"`
    Password  string `json:"password"`
    Status    uint8  `json:"status"`
    CreatedAt string `json:"created_at"`
    UpdatedAt string `json:"updated_at"`
}

var ErrNotFound = errors.New("Resource not found")

const (
    INSERT_USER             = "INSERT INTO users (nickname, email, password) VALUES ($1, $2, $3) RETURNING uid"
    INSERT_WALLET           = "INSERT INTO wallets (public_key, usr) VALUES ($1, $2)"
    USERS                   = "SELECT * FROM users"
    FETCH_ONE_USER          = "SELECT * FROM users WHERE uid = $1"
    FETCH_ONE_USER_BY_EMAIL = "SELECT * FROM users WHERE email = $1"
    UPDATE_USER             = "UPDATE users SET nickname = $1, email = $2, status = $3 WHERE uid = $4"
    DELETE_USER             = "DELETE FROM users WHERE uid = $1"
)

func NewUser(user User) (bool, error) {
    db := Connect()
    defer db.Close()

    tx, err := db.Begin()
    if err != nil {
        return false, err
    }
    {
        stmt, err := tx.Prepare(INSERT_USER)
        if err != nil {
            tx.Rollback()
            return false, err
        }
        defer stmt.Close()
        hashedPassword, err := utils.Bcrypt(user.Password)
        if err != nil {
            tx.Rollback()
            return false, err
        }
        err = stmt.QueryRow(user.Nickname, user.Email, hashedPassword).Scan(&user.UID)
        if err != nil {
            tx.Rollback()
            return false, err
        }
    }
    wallet := Wallet{User: user}
    wallet.GeneratePublicKey()
    {
        stmt, err := tx.Prepare(INSERT_WALLET)
        if err != nil {
            tx.Rollback()
            return false, err
        }
        _, err = stmt.Exec(wallet.PublicKey, wallet.User.UID)
        if err != nil {
            return false, err
        }
    }
    return true, tx.Commit()
}

func GetUsers() ([]User, error) {
    db := Connect()
    defer db.Close()
    rows, err := db.Query(USERS)
    if err != nil {
        return nil, err
    }
    defer rows.Close()

    var users []User
    for rows.Next() {
        var user User
        err := rows.Scan(&user.UID, &user.Nickname, &user.Email, &user.Password, &user.Status, &user.CreatedAt, &user.UpdatedAt)
        if err != nil {
            return nil, err
        }
        users = append(users, user)
    }
    return users, nil
}

func GetOneUser(uid uint) (User, error) {
    db := Connect()
    defer db.Close()
    row := db.QueryRow(FETCH_ONE_USER, uid)
    var user User
    err := row.Scan(&user.UID, &user.Nickname, &user.Email, &user.Password, &user.Status, &user.CreatedAt, &user.UpdatedAt)
    if err != nil {
        return User{}, ErrNotFound
    }
    return user, nil
}

func GetUserByEmail(email string) (User, error) {
    db := Connect()
    defer db.Close()
    row := db.QueryRow(FETCH_ONE_USER_BY_EMAIL, email)
    var user User
    err := row.Scan(&user.UID, &user.Nickname, &user.Email, &user.Password, &user.Status, &user.CreatedAt, &user.UpdatedAt)
    if err != nil {
        return User{}, ErrNotFound
    }
    return user, nil
}

func PutOneUser(uid uint, user User) (User, error) {
    db := Connect()
    defer db.Close()
    stmt, err := db.Prepare(UPDATE_USER)
    if err != nil {
        return User{}, err
    }
    defer stmt.Close()

    _, err = stmt.Exec(user.Nickname, user.Email, user.Status, uid)
    if err != nil {
        return User{}, err
    }
    return User{}, nil
}

func DeleteUser(uid uint) error {
    db := Connect()
    defer db.Close()
    stmt, err := db.Prepare(DELETE_USER)
    if err != nil {
        return err
    }
    defer stmt.Close()

    _, err = stmt.Exec(uid)
    if err != nil {
        return err
    }
    return nil
}
