package models

import (
    "database/sql"
    "fmt"
    _ "github.com/lib/pq"
    "log"
)

const (
    USER    = "postgres"
    PASS    = "postgres_password"
    DBNAME  = "blockcoin"
    SSLMODE = "disable"
)

func Connect() *sql.DB {
    URL := fmt.Sprintf(
        "postgres://%s:%s@127.0.0.1/%s?sslmode=%s",
        USER,
        PASS,
        DBNAME,
        SSLMODE,
    )
    db, err := sql.Open("postgres", URL)
    if err != nil {
        log.Fatal(err.Error())
    }

    return db
}

func TestConnection() {
    c := Connect()
    defer c.Close()
    err := c.Ping()
    if err != nil {
        log.Fatal(err.Error())
    }
    fmt.Println("Database connected!")
}
