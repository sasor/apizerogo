package models

type Transaction struct {
    UID       uint    `json:"_id"`
    Origin    Wallet  `json:"origin"`
    Target    Wallet  `json:"target"`
    Cash      float32 `json:"cash"`
    Message   string  `json:"message"`
    CreatedAt string  `json:"created_at"`
    UpdatedAt string  `json:"updated_at"`
}

const (
    NEW_TRANSACTION_MINUS = "UPDATE wallets SET balance = (balance - $1) WHERE  public_key = $2"
    NEW_TRANSACTION_PLUS  = "UPDATE wallets SET balance = (balance + $1) WHERE  public_key = $2"
    NEW_TRANSACTION       = "INSERT INTO transactions (origin, target, cash, message) VALUES ($1, $2, $3, $4)"
    FETCH_TRANSACTIONS    = "SELECT * FROM transactions"
)

func NewTransaction(t Transaction) (bool, error) {
    db := Connect()
    defer db.Close()

    tx, err := db.Begin()
    if err != nil {
        return false, err
    }
    {
        stmt, err := tx.Prepare(NEW_TRANSACTION_MINUS)
        if err != nil {
            tx.Rollback()
            return false, err
        }
        defer stmt.Close()

        _, err = stmt.Exec(t.Origin.Balance, t.Origin.PublicKey)
        if err != nil {
            tx.Rollback()
            return false, err
        }
    }
    {
        stmt, err := tx.Prepare(NEW_TRANSACTION_PLUS)
        if err != nil {
            tx.Rollback()
            return false, err
        }
        defer stmt.Close()

        _, err = stmt.Exec(t.Origin.Balance, t.Target.PublicKey)
        if err != nil {
            tx.Rollback()
            return false, err
        }
    }
    {
        stmt, err := tx.Prepare(NEW_TRANSACTION)
        if err != nil {
            tx.Rollback()
            return false, err
        }
        defer stmt.Close()

        _, err = stmt.Exec(t.Origin.PublicKey, t.Target.PublicKey, t.Cash, t.Message)
        if err != nil {
            tx.Rollback()
            return false, err
        }
    }
    return true, tx.Commit()
}

func GetTransactions() ([]Transaction, error) {
    db := Connect()
    defer db.Close()

    rows, err := db.Query(FETCH_TRANSACTIONS)
    if err != nil {
        return nil, err
    }
    defer rows.Close()

    var transactions []Transaction
    for rows.Next() {
        var transaction Transaction
        rows.Scan(
            &transaction.UID,
            &transaction.Origin.PublicKey,
            &transaction.Target.PublicKey,
            &transaction.Cash,
            &transaction.Message,
            &transaction.CreatedAt,
            &transaction.UpdatedAt,
        )
        transactions = append(transactions, transaction)
    }
    return transactions, nil
}
