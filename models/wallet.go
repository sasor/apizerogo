package models

import (
    "errors"
    "fmt"
    "github.com/sasor/apizeroingo/utils"
)

type Wallet struct {
    PublicKey string  `json:"public_key"`
    User      User    `json:"user"`
    Balance   float32 `json:"balance"`
    UpdatedAt string  `json:"updated_at"`
}

const (
    INDEX_WALLETS = `
        SELECT u.uid, u.nickname, u.email, u.status, u.created_at, u.updated_at, w.public_key, w.balance
        FROM wallets AS w
        INNER JOIN users as u ON u.uid = w.usr
        ORDER BY w.usr ASC
    `
    WALLET_BY_PK = `
        SELECT u.uid, u.nickname, u.email, u.status, u.created_at, u.updated_at, w.public_key, w.balance
        FROM wallets AS w
        INNER JOIN users as u ON u.uid = w.usr
        WHERE public_key = $1
    `
    UPDATE_ONE_WALLET = "UPDATE wallets SET balance = $1 WHERE public_key = $2"
    BALANCE_WALLET    = "UPDATE wallets SET balance = (balance + $1) WHERE public_key = $2"
)

var (
    ErrNotFoundResource = errors.New("Resource not found")
)

func (w *Wallet) GeneratePublicKey() {
    param := fmt.Sprintf("%s%s", w.User.Nickname, w.User.Password)
    w.PublicKey = utils.Md5(param)
}

func GetWallets() ([]Wallet, error) {
    db := Connect()
    defer db.Close()

    rows, err := db.Query(INDEX_WALLETS)
    if err != nil {
        return nil, err
    }
    defer rows.Close()

    var wallets []Wallet
    for rows.Next() {
        var wallet Wallet
        err := rows.Scan(
            &wallet.User.UID,
            &wallet.User.Nickname,
            &wallet.User.Email,
            &wallet.User.Status,
            &wallet.User.CreatedAt,
            &wallet.User.UpdatedAt,
            &wallet.PublicKey,
            &wallet.Balance,
        )
        if err != nil {
            return nil, err
        }
        wallets = append(wallets, wallet)
    }
    return wallets, nil
}

func GetWalletByPublicKey(publicKey string) (Wallet, error) {
    db := Connect()
    defer db.Close()

    row := db.QueryRow(WALLET_BY_PK, publicKey)
    var wallet Wallet
    err := row.Scan(
        &wallet.User.UID,
        &wallet.User.Nickname,
        &wallet.User.Email,
        &wallet.User.Status,
        &wallet.User.CreatedAt,
        &wallet.User.UpdatedAt,
        &wallet.PublicKey,
        &wallet.Balance,
    )
    if err != nil {
        return Wallet{}, ErrNotFoundResource
    }
    return wallet, nil
}

func UpdateOneWallet(w Wallet) (int64, error) {
    db := Connect()
    defer db.Close()

    stmt, err := db.Prepare(UPDATE_ONE_WALLET)
    if err != nil {
        return 0, err
    }
    defer stmt.Close()

    rs, err := stmt.Exec(w.Balance, w.PublicKey)
    if err != nil {
        return 0, err
    }
    return rs.RowsAffected()
}

func AddBalanceToWallet(w Wallet) (int64, error) {
    db := Connect()
    defer db.Close()

    stmt, err := db.Prepare(BALANCE_WALLET)
    if err != nil {
        return 0, err
    }
    defer stmt.Close()

    rs, err := stmt.Exec(w.Balance, w.PublicKey)
    if err != nil {
        return 0, err
    }
    return rs.RowsAffected()
}
