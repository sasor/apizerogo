package controllers

import (
    "github.com/sasor/apizeroingo/utils"
    "net/http"
)

func HomeController(w http.ResponseWriter, _ *http.Request) {
    utils.ToJson(w, struct {
        Message string `json:"message"`
    }{
        Message: "Hello world!",
    })
}
