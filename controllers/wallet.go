package controllers

import (
    "encoding/json"
    "github.com/gorilla/mux"
    "github.com/sasor/apizeroingo/models"
    "github.com/sasor/apizeroingo/utils"
    "io/ioutil"
    "net/http"
)

func GetWallets(w http.ResponseWriter, _ *http.Request) {
    wallets, err := models.GetWallets()
    if err != nil {
        utils.ErrorResponse(w, err, http.StatusUnprocessableEntity)
        return
    }
    utils.ToJson(w, wallets)
}

func GetOneWalletByPublicKey(w http.ResponseWriter, r *http.Request) {
    params := mux.Vars(r)
    publicKey := params["public_key"]
    wallet, err := models.GetWalletByPublicKey(publicKey)
    if err != nil {
        utils.ErrorResponse(w, err, http.StatusUnprocessableEntity)
        return
    }
    utils.ToJson(w, wallet)
}

func UpdateOneWallet(w http.ResponseWriter, r *http.Request) {
    params := mux.Vars(r)
    var publicKey string = params["public_key"]
    body, _ := ioutil.ReadAll(r.Body)
    var wallet models.Wallet
    err := json.Unmarshal(body, &wallet)
    if err != nil {
        utils.ErrorResponse(w, err, http.StatusUnprocessableEntity)
        return
    }
    wallet.PublicKey = publicKey
    rs, err := models.UpdateOneWallet(wallet)
    if err != nil {
        utils.ErrorResponse(w, err, http.StatusUnprocessableEntity)
        return
    }
    utils.ToJson(w, rs)
}

func UpdateBalanceWallet(w http.ResponseWriter, r *http.Request) {
    params := mux.Vars(r)
    var publicKey string = params["public_key"]
    body, _ := ioutil.ReadAll(r.Body)
    var wallet models.Wallet
    err := json.Unmarshal(body, &wallet)
    if err != nil {
        utils.ErrorResponse(w, err, http.StatusUnprocessableEntity)
        return
    }
    wallet.PublicKey = publicKey
    rs, err := models.AddBalanceToWallet(wallet)
    if err != nil {
        utils.ErrorResponse(w, err, http.StatusUnprocessableEntity)
        return
    }
    utils.ToJson(w, rs)
}
