package controllers

import (
    "encoding/json"
    "errors"
    "fmt"
    "github.com/gorilla/mux"
    "github.com/sasor/apizeroingo/models"
    "github.com/sasor/apizeroingo/utils"
    "github.com/sasor/apizeroingo/validations"
    "io/ioutil"
    "net/http"
)

func PostTransaction(w http.ResponseWriter, r *http.Request) {
    transaction, err := verifyTransaction(r)
    if err != nil {
        utils.ErrorResponse(w, err, http.StatusUnprocessableEntity)
        return
    }
    _, err = models.NewTransaction(transaction)
    if err != nil {
        utils.ErrorResponse(w, err, http.StatusUnprocessableEntity)
        return
    }
    utils.ToJson(w, utils.DefaultResponse{Data: "Transaction successfully", Status: http.StatusCreated})
}

func verifyTransaction(r *http.Request) (models.Transaction, error) {
    params := mux.Vars(r)
    targetPk := params["public_key"]
    target, err := models.GetWalletByPublicKey(targetPk)
    if err != nil {
        return models.Transaction{}, err
    }
    body, _ := ioutil.ReadAll(r.Body)
    var originRequest models.Wallet
    err = json.Unmarshal(body, &originRequest)
    if err != nil {
        return models.Transaction{}, err
    }
    originStored, err := models.GetWalletByPublicKey(originRequest.PublicKey)
    if err != nil {
        return models.Transaction{}, err
    }
    if validations.IsEmpty(target.PublicKey) || validations.IsEmpty(originStored.PublicKey) {
        return models.Transaction{}, models.ErrNotFoundResource
    }
    // Check balance
    if originRequest.Balance > originStored.Balance || originRequest.Balance < 0 {
        return models.Transaction{}, errors.New("Invalid cash, balance insuficient! or Origin Balance is zero")
    }
    transaction := models.Transaction{
        Origin:  originRequest,
        Target:  target,
        Cash:    originRequest.Balance,
        Message: fmt.Sprintf("%s transfer %.2f, to %s", originStored.User.Nickname, originRequest.Balance, target.User.Nickname),
    }
    return transaction, nil
}

func GetTransactions(w http.ResponseWriter, _ *http.Request) {
    transactions, err := models.GetTransactions()
    if err != nil {
        utils.ErrorResponse(w, err, http.StatusUnprocessableEntity)
        return
    }
    utils.ToJson(w, transactions)
}
