package controllers

import (
    "encoding/json"
    "github.com/sasor/apizeroingo/auth"
    "github.com/sasor/apizeroingo/models"
    "github.com/sasor/apizeroingo/utils"
    "io"
    "net/http"
)

func Login(w http.ResponseWriter, r *http.Request) {
    body, _ := io.ReadAll(r.Body)
    var u models.User
    err := json.Unmarshal(body, &u)
    if err != nil {
        utils.ErrorResponse(w, err, http.StatusBadRequest)
        return
    }
    signedUser, err := auth.SignIn(u)
    if err != nil {
        utils.ErrorResponse(w, err, http.StatusUnauthorized)
        return
    }
    utils.ToJson(w, signedUser)
}
