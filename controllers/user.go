package controllers

import (
    "encoding/json"
    "github.com/gorilla/mux"
    "github.com/sasor/apizeroingo/models"
    "github.com/sasor/apizeroingo/utils"
    "github.com/sasor/apizeroingo/validations"
    "io/ioutil"
    "net/http"
    "strconv"
)

func IndexUsers(w http.ResponseWriter, _ *http.Request) {
    users, err := models.GetUsers()
    if err != nil {
        utils.ErrorResponse(w, err, http.StatusBadRequest)
        return
    }
    utils.ToJson(w, users)
}

func CreateUser(w http.ResponseWriter, r *http.Request) {
    body, _ := ioutil.ReadAll(r.Body)
    var user models.User
    err := json.Unmarshal(body, &user)
    if err != nil {
        utils.ErrorResponse(w, err, http.StatusBadRequest)
        return
    }
    user, err = validations.User(user)
    if err != nil {
        utils.ErrorResponse(w, err, http.StatusBadRequest)
        return
    }

    _, err = models.NewUser(user)
    if err != nil {
        utils.ErrorResponse(w, err, http.StatusBadRequest)
        return
    }
    utils.ToJson(w, utils.DefaultResponse{Data: "user created", Status: http.StatusCreated})
}

func FetchUser(w http.ResponseWriter, r *http.Request) {
    param := mux.Vars(r)
    uid, _ := strconv.ParseUint(param["uid"], 10, 32)
    user, err := models.GetOneUser(uint(uid))
    if err != nil {
        utils.ErrorResponse(w, err, http.StatusBadRequest)
        return
    }
    utils.ToJson(w, user)
}

func UpdateUser(w http.ResponseWriter, r *http.Request) {
    params := mux.Vars(r)
    uid, _ := strconv.ParseUint(params["uid"], 10, 32)
    body, _ := ioutil.ReadAll(r.Body)
    var user models.User
    err := json.Unmarshal(body, &user)
    if err != nil {
        utils.ErrorResponse(w, err, http.StatusBadRequest)
        return
    }
    user, err = models.PutOneUser(uint(uid), user)
    if err != nil {
        utils.ErrorResponse(w, err, http.StatusBadRequest)
        return
    }
    utils.ToJson(w, user)
}

func DeleteOneUser(w http.ResponseWriter, r *http.Request) {
    params := mux.Vars(r)
    uid, _ := strconv.ParseUint(params["uid"], 10, 32)
    err := models.DeleteUser(uint(uid))
    if err != nil {
        utils.ErrorResponse(w, err, http.StatusUnprocessableEntity)
        return
    }
    w.WriteHeader(http.StatusNoContent)
}
