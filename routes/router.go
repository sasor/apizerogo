package routes

import (
    "github.com/gorilla/mux"
    "github.com/sasor/apizeroingo/controllers"
    "github.com/sasor/apizeroingo/middlewares"
)

func NewRouter() *mux.Router {
    r := mux.NewRouter().StrictSlash(true)
    r.HandleFunc("/", controllers.HomeController).Methods("GET")
    // User Routes
    r.HandleFunc("/users", middlewares.IsAuth(controllers.IndexUsers)).Methods("GET")
    r.HandleFunc("/users", controllers.CreateUser).Methods("POST")
    r.HandleFunc("/users/{uid}", middlewares.IsAuth(controllers.FetchUser)).Methods("GET")
    r.HandleFunc("/users/{uid}", middlewares.IsAuth(controllers.UpdateUser)).Methods("PUT")
    r.HandleFunc("/users/{uid}", middlewares.IsAuth(controllers.DeleteOneUser)).Methods("DELETE")
    // Wallet Routes
    r.HandleFunc("/wallets", middlewares.IsAuth(controllers.GetWallets)).Methods("GET")
    r.HandleFunc("/wallets/{public_key}", middlewares.IsAuth(controllers.GetOneWalletByPublicKey)).Methods("GET")
    r.HandleFunc("/wallets/{public_key}", middlewares.IsAuth(controllers.UpdateOneWallet)).Methods("PUT")
    r.HandleFunc("/wallets/{public_key}/add", middlewares.IsAuth(controllers.UpdateBalanceWallet)).Methods("PUT")
    // Transactions
    r.HandleFunc("/transactions", controllers.GetTransactions).Methods("GET")
    r.HandleFunc("/transactions/{public_key}", controllers.PostTransaction).Methods("POST")
    // Login
    r.HandleFunc("/login", controllers.Login).Methods("POST")
    return r
}
