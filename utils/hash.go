package utils

import (
    "crypto/md5"
    "fmt"
    "golang.org/x/crypto/bcrypt"
)

func Md5(param string) string {
    return fmt.Sprintf("%x", md5.Sum([]byte(param)))
}

func Bcrypt(password string) ([]byte, error) {
    return bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
}

// IsPassword return error equals nil on success
func IsPassword(hash string, password string) error {
    return bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
}
