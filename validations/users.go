package validations

import (
    "errors"
    "github.com/sasor/apizeroingo/models"
)

var (
    EmptyField = errors.New("field cannot be blank")
)

func User(user models.User) (models.User, error) {
    if IsEmpty(user.Nickname) || IsEmpty(user.Email) || IsEmpty(user.Password) {
        return models.User{}, EmptyField
    }
    return user, nil
}
