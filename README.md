# Api zero go
Practica con el lenguaje golang,creando una API

#### Ejecutar sin docker

```shell
go run api.go
```

## ENDPOINTS

### Login
- login user using email and password
```shell
POST - curl -X POST --data '{"email":"XXX", "password":10}'  [_HOST]:[_PORT]/login 
```

#### Users
- Fetch all users
```shell
GET - curl [_HOST]:[_PORT]/users
```
- Create a new user
```shell
POST - curl -X POST --data @seed/user.json [_HOST]:[_PORT]/users 
```
- Update one user
```shell
PUT - curl -X PUT --data @seed/user.json [_HOST]:[_PORT]/users/{uid} 
```
- Delete one user
```shell
DELETE - curl -X DELETE -I [_HOST]:[_PORT]/users/{uid}
```

#### Wallets
- Fetch all wallets
```shell
GET - curl [_HOST]:[_PORT]/wallets
```
- Fetch one wallet using public_key field
```shell
GET - curl [_HOST]:[_PORT]/wallets/{public_key}
```
- Update one wallet
```shell
PUT - curl -X PUT [_HOST]:[_PORT]/wallets/{public_key} --data '{"balance":12}'
```
- Increase balance
```shell
PUT - curl -X PUT [_HOST]:[_PORT]/wallets/{public_key}/add --data '{"balance":12}'
```

#### Transactions
- Fetch all transactions
```shell
GET - curl [_HOST]:[_PORT]/transactions
```
- Create a new transaction
```shell
POST - curl -X POST [_HOST]:[_PORT]/transactions/{public_key} --data '{"public_key":"XXX", "balance":10}'
```

##### Thanks,Agradecimientos
```http request
https://www.youtube.com/playlist?list=PLFXr22TafQUtTyYxq3LG0bcgnI9DI2J_9
```