package auth

import (
    "errors"
    "github.com/golang-jwt/jwt"
    "github.com/sasor/apizeroingo/models"
    "github.com/sasor/apizeroingo/utils"
    "time"
)

const (
    SECRET_KEY = "asdf1234"
)

var (
    ErrInvalidCredentials = errors.New("Invalid Email/Password as credentials")
)

type Auth struct {
    User    models.User `json:"user"`
    Token   string      `json:"token"`
    IsValid bool        `json:"is_valid"`
}

func SignIn(u models.User) (Auth, error) {
    pwd := u.Password
    userStored, err := models.GetUserByEmail(u.Email)
    if err != nil {
        return Auth{IsValid: false}, ErrInvalidCredentials
    }
    err = utils.IsPassword(userStored.Password, pwd)
    if err != nil {
        return Auth{IsValid: false}, ErrInvalidCredentials
    }

    tokenJWT, err := GenerateTokenJWT(userStored)
    if err != nil {
        return Auth{IsValid: false}, err
    }

    return Auth{User: userStored, Token: tokenJWT, IsValid: true}, nil
}

func GenerateTokenJWT(u models.User) (string, error) {
    token := jwt.New(jwt.SigningMethodHS256)
    claims := token.Claims.(jwt.MapClaims)
    claims["authorized"] = true
    claims["userId"] = u.UID
    claims["exp"] = time.Now().Add(time.Second * 15).Unix()
    return token.SignedString([]byte(SECRET_KEY))
}
