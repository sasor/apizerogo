package middlewares

import (
    "errors"
    "fmt"
    "github.com/golang-jwt/jwt"
    "github.com/sasor/apizeroingo/auth"
    "github.com/sasor/apizeroingo/utils"
    "log"
    "net/http"
    "strings"
)

func IsAuth(f http.HandlerFunc) http.HandlerFunc {
    return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
        header := r.Header.Get("Authorization")
        log.Print(header)
        if header != "" {
            bearerToken := strings.Split(header, " ")
            log.Print(bearerToken)
            if len(bearerToken) == 2 {
                token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
                    _, ok := token.Method.(*jwt.SigningMethodHMAC)
                    if !ok {
                        return nil, fmt.Errorf("%s", "Error authentification")
                    }
                    return []byte(auth.SECRET_KEY), nil
                })
                if err != nil {
                    utils.ErrorResponse(w, errors.New("unauthorized"), http.StatusUnauthorized)
                    return
                }
                if token.Valid {
                    f(w, r)
                }
            }
        }
        utils.ErrorResponse(w, errors.New("unauthorized user"), http.StatusUnauthorized)
    })
}
