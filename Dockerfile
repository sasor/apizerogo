FROM golang:1.17.10 AS builder
ENV GOOS=linux \
    GOARCH=amd64 \
WORKDIR /go/src/app
COPY go.mod go.sum ./
RUN go mod download
COPY . ./
RUN go install

#FROM golang:1.17.10-alpine3.15
FROM scratch
ENV PORT=3000 \
    HOST=0.0.0.0
COPY --from=builder /go/bin/apizeroingo ./
ENTRYPOINT ["./apizeroingo"]
