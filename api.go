package main

import (
    "fmt"
    "github.com/sasor/apizeroingo/models"
    "github.com/sasor/apizeroingo/routes"
    "log"
    "net/http"
    "os"
)

func main() {
    var port, host string
    port = os.Getenv("PORT")
    host = os.Getenv("HOST")

    models.TestConnection()
    r := routes.NewRouter()
    fmt.Printf("Api running on port %s", port)
    log.Fatal(http.ListenAndServe(fmt.Sprintf("%s:%s", host, port), r))
}
